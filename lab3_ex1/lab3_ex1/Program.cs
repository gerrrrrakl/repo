﻿using System;

namespace lab3_ex1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите x:");
            double x = Convert.ToDouble(Console.ReadLine());
            double y = 0;
            if ( x > 20 ) 
            y = x - 5 / x;

            if ( x <= 20 & x >= 0 ) 
            y = 1 - 3 * x;

            if ( x < 0 )
            y = 2 * x;

            Console.WriteLine("y=" + y);

        }
    }
}
