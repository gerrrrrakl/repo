﻿using System;

namespace lab3_ex._2
{
    class Program
    {
        static void Main(string[] args)
        {
            // f(x, y, z) = !x|!z&z&y = true
            bool x = false;
            bool z = true;
            bool y = true;

            var f = !x | !z & z & y;

            Console.WriteLine("f=" + f);


        }
    }
}
