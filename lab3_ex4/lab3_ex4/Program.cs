﻿using System;

namespace lab3_ex4
{
    class Program
    {
        static void Main(string[] args)
        {
            int n, m, g;
            Console.WriteLine("Введите день");
            n = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Введите месяц");
            m = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Введите год");
            g = Convert.ToInt32(Console.ReadLine());

            DateTime date1 = new DateTime(g, m, n);
            DateTime date0 = new DateTime(g, m, n).AddDays(-1);
            DateTime date2 = new DateTime(g, m, n).AddDays(1);

            Console.WriteLine("текущий день:" + date1.ToShortDateString());
            Console.WriteLine("Предыдущий день:" + date0.ToShortDateString());
            Console.WriteLine("Следующий день:" + date2.ToShortDateString());

            Console.ReadLine();
        }
    }
}
